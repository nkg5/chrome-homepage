var weather_temp = document.getElementById("weather-temp");
var weather_condition = document.getElementById("weather-condition");
var inbox_subjects = document.getElementsByClassName("inbox-subject");
var inbox_senders = document.getElementsByClassName("inbox-sender");
function updateWidgets() {
    var get_weather = new XMLHttpRequest(),
        get_inbox = new XMLHttpRequest();
    get_weather.onreadystatechange = function () {
        if (get_weather.readyState === 4 && get_weather.status === 200) {
            var response = JSON.parse(get_weather.responseText), 
                id = response['weather'][0]["id"], 
                temp = response["main"]["temp"];
            temp = temp - 273.15;
            temp = Math.round(temp);
            weather_temp.innerHTML = temp + "&degC";
            if (id >= 200 && id < 300) {
                weather_condition.src = "assets/images/Thunderstorm.png";
            } else if (id >= 500 && id < 600) {
                weather_condition.src = "assets/images/Shower.png";
            } else if (id >= 600 && id < 700) {
                weather_condition.src = "assets/images/Snow.png";
            } else if (id > 700 && id < 800) {
                weather_condition.src = "assets/images/Mist.png";
            } else if (id > 800 && id < 900) {
                weather_condition.src = "assets/images/Cloudy.png";
            } else if (id === 800) {
                weather_condition.src = "assets/images/Sunny.png";
            }
        }
    };
    get_weather.open("GET", "http://localhost:4040/weather", true);
    get_weather.send();
    get_inbox.onreadystatechange = function () {
        if (get_inbox.readyState === 4 && get_inbox.status === 200) {
            var xmlDoc, i, fullcount, parser;
            if (window.DOMParser) {
                parser = new DOMParser();
                xmlDoc = parser.parseFromString(get_inbox.responseText, "application/xml");
            } else {
                xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
                xmlDoc.async = false;
                xmlDoc.loadXML(get_inbox.responseText);
            }
            fullcount = xmlDoc.getElementsByTagName("fullcount")[0].childNodes[0].nodeValue;
            if (fullcount > 4) {
                fullcount = 4;
            }
            for (i = 0; i < fullcount; i += 1) {
                inbox_subjects[i].innerHTML = xmlDoc.getElementsByTagName("title")[i + 1].childNodes[0].nodeValue;
                inbox_senders[i].innerHTML = xmlDoc.getElementsByTagName("email")[i].childNodes[0].nodeValue.substring(0, 35);
            }
        }
    };
    get_inbox.open("GET", "http://localhost:4040/mail", true);
    get_inbox.send();
}
