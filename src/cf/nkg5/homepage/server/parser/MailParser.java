package cf.nkg5.homepage.server.parser;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

public class MailParser {
	public MailParser(PrintWriter out, String username, String password){
		String line="";
		try{
			System.out.println("Got request for mail.");
			URL url = new URL("https://mail.google.com/mail/feed/atom");
			URLConnection uc = url.openConnection();
			String userpass = username + ":" + password;
			String basicAuth = "Basic " + new String(Base64.encode(userpass.getBytes()));
			uc.setRequestProperty ("Authorization", basicAuth);
			InputStream is = uc.getInputStream();
			BufferedReader reader=new BufferedReader(new InputStreamReader(is));
			line=reader.readLine();
			System.out.println("Sending data: "+line);
			out.println("HTTP/1.1 200 OK");
			out.println("Content-Type: application/xml; charset=utf-8");
			out.println("Content-Length: "+line.length());
			out.println("Access-Control-Allow-Origin: *");
			out.println("");
		    out.println(line);
		    out.flush();
			
			}catch(Exception e){
				System.err.println(e.getClass().toString().substring(6)+": "+ e.getMessage());
				out.println("HTTP/1.1 503 Service Unavailable");
				out.println("Content-Type: application/json; charset=utf-8");
				out.println("Content-Length: "+line.length());
				out.println("Access-Control-Allow-Origin: *");
				out.println("");
			    out.println(line);
			    out.flush();
			}
	}
}
