package cf.nkg5.homepage.server.parser;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import cf.nkg5.homepage.server.ServerMain;

public class IndexParser {
	public IndexParser(PrintWriter out){
		String line="";
		try{
			BufferedReader in=new BufferedReader(new InputStreamReader(ServerMain.class.getResourceAsStream("/www/index.html")));
			StringBuilder s=new StringBuilder();
			while((line=in.readLine())!=null){
				s.append(line);
			}
			out.println("HTTP/1.1 200 OK");
			out.println("Content-Type: text/html; charset=utf-8");
			out.println("Content-Length: "+s.toString().length());
			out.println("Access-Control-Allow-Origin: *");
			out.println("");
		    out.println(s.toString());
		    out.flush();
		}catch(Exception e){
			System.err.println(e.getClass().toString().substring(6)+": "+ e.getMessage());
			out.println("HTTP/1.1 503 Service Unavailable");
			out.println("Content-Type: application/json; charset=utf-8");
			out.println("Content-Length: "+line.length());
			out.println("Access-Control-Allow-Origin: *");
			out.println("");
		    out.println(line);
		    out.flush();
		}
	}
}
