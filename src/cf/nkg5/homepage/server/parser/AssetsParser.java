package cf.nkg5.homepage.server.parser;

import java.io.File;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.file.Files;

public class AssetsParser {
	public AssetsParser(PrintWriter out, OutputStream os, String asset){
		String line="/www"+asset;
		try{
			File file=new File(AssetsParser.class.getResource(line).toURI());
			System.out.println(Files.size(file.toPath()));
			out.println("HTTP/1.1 200 OK");
			if(asset.endsWith(".css"))
				out.println("Content-Type: text/css; charset=utf-8");
			else if(asset.endsWith(".css"))
				out.println("Content-Type: image/png; charset=utf-8");
			out.println("Content-Length: "+Files.size(file.toPath()));
			out.println("Access-Control-Allow-Origin: *");
			out.println("");
			out.flush();
			Files.copy(file.toPath(), os);
			os.flush();
		}catch(Exception e){
			System.err.println(e.getClass().toString().substring(6)+": "+ e.getMessage());
			System.out.println(line);
			out.println("HTTP/1.1 503 Service Unavailable");
			out.println("Access-Control-Allow-Origin: *");
			out.println("");
		    out.flush();
		}
	}
}
