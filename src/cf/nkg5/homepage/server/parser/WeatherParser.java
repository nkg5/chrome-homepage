package cf.nkg5.homepage.server.parser;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;

public class WeatherParser {
	public WeatherParser(PrintWriter out, String city, String appid){
		String line="";
		try{
			System.out.println("Got request for weather.");
			URL u = new URL("http://api.openweathermap.org/data/2.5/weather?q="+city+"&appid="+appid);
			InputStream is=u.openStream();
			BufferedReader reader=new BufferedReader(new InputStreamReader(is));
			line=reader.readLine();
			System.out.println("Sending data: "+line);
			out.println("HTTP/1.1 200 OK");
			out.println("Content-Type: application/json; charset=utf-8");
			out.println("Content-Length: "+line.length());
			out.println("Access-Control-Allow-Origin: *");
			out.println("");
		    out.println(line);
		    out.flush();
		}catch(Exception e){
			System.err.println(e.getClass().toString().substring(6)+": "+ e.getMessage());
			out.println("HTTP/1.1 503 Service Unavailable");
			out.println("Content-Type: application/json; charset=utf-8");
			out.println("Content-Length: "+line.length());
			out.println("Access-Control-Allow-Origin: *");
			out.println("");
		    out.println(line);
		    out.flush();
		}
	}
}
