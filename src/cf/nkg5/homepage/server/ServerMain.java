package cf.nkg5.homepage.server;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerMain {

	private static String line = "";
	private static String appid = "";
	private static String city = "";
	private static String username = "";
	private static String password = "";
	private static BufferedReader in;
	private static boolean running=true;
	public static void main(String[] args) {
		try {
			in=new BufferedReader(new InputStreamReader(ServerMain.class.getResourceAsStream("/config.cfg")));
			while((line=in.readLine())!=null){
				if(line.startsWith("appid")){
					String[] tmp=line.split("=");
					appid=tmp[1].trim();
				}else if(line.startsWith("city")){
					String[] tmp=line.split("=");
					city=tmp[1].trim();
				}else if(line.startsWith("username")){
					String[] tmp=line.split("=");
					username=tmp[1].trim();
				}else if(line.startsWith("password")){
					String[] tmp=line.split("=");
					password=tmp[1].trim();
				}
			}
			ServerFrame.create();
			ServerSocket server = new ServerSocket(4040);
			while(running){
				Socket socket=server.accept();
				Thread t=new Thread(new RequestHandler(socket, city, appid, username, password));
				t.start();
			}
			server.close();
		} catch (Exception e) {
			System.err.println(e.getClass().toString().substring(6)+": "+ e.getMessage());
		}
	}
}
