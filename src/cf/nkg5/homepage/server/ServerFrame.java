package cf.nkg5.homepage.server;

import java.awt.Dimension;
import java.awt.Font;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.UIManager;

public class ServerFrame{
	private static JFrame window;
	private static JTextArea mTextArea;
	private static LogOutputStream outputStream;
	
	public static void create(){
		// Set system look and feel
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		// Create frame
		window=new JFrame("Chrome Homepage");
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mTextArea=new JTextArea();
		mTextArea.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 12));
		mTextArea.setEditable(false);
		JScrollPane scrollPane=new JScrollPane(mTextArea);
		scrollPane.setPreferredSize(new Dimension(480, 280));
		window.add(scrollPane);
		window.pack();
		// Show frame
		window.setVisible(true);
		
		// Setup stream
		outputStream=new LogOutputStream();
		PrintStream out=new PrintStream(outputStream);
		System.setErr(out);
	}
	
	protected static class LogOutputStream extends OutputStream{

		@Override
		public void write(int arg0) throws IOException {
			mTextArea.append(Character.toString(((char)arg0)));
			window.revalidate();
		}

	}
}

