package cf.nkg5.homepage.server;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import cf.nkg5.homepage.server.parser.AssetsParser;
import cf.nkg5.homepage.server.parser.IndexParser;
import cf.nkg5.homepage.server.parser.MailParser;
import cf.nkg5.homepage.server.parser.WeatherParser;

public class RequestHandler implements Runnable {
	private Socket socket;
	private BufferedReader in;
	private String line,city,appid,username,password;
	
	public RequestHandler(Socket socket,String city, String appid, String username, String password) {
		this.socket=socket;
		this.city=city;
		this.appid=appid;
		this.username=username;
		this.password=password;
	}
	
	@Override
	public void run() {
		try {
			in=new BufferedReader(new InputStreamReader(socket.getInputStream()));
			PrintWriter out = new PrintWriter(socket.getOutputStream());
			while((line=in.readLine())!=null){
				if(line.contains("GET /weather HTTP/1.1")){
					new WeatherParser(out, city, appid);
				}else if(line.contains("GET /mail HTTP/1.1")){
					new MailParser(out, username, password);
				}else if(line.startsWith("GET /assets/")&&line.endsWith("HTTP/1.1")){
					String[] tmp=line.split(" ");
					new AssetsParser(out,socket.getOutputStream(), tmp[1]);
				}else if(line.startsWith("GET ")&&line.endsWith("HTTP/1.1")){
					new IndexParser(out);
				}
			}
		} catch (Exception e) {
			System.err.println(e.getClass().toString().substring(6)+": "+ e.getMessage());
		}
	}

}
