# Chrome Homepage #

This is homepage dashboard that is a personal project of mine.
Main reason behind me creating this page is that Google Chrome loads local html pages faster than it's New Tab page.

### What does it currently contains ###

* Google Search header.
* Bookmark icons.
* Weather widget.
* Inbox widget
* Custom local server to go with it (faster than using external web server).

### How to use it ###
You need to make those steps to be able to use webpage to it's full potential.

1. Rename config.dist.cfg from src to config.cfg
2. Get your weather appid from: http://openweathermap.org/
3. Fill up config.cfg (step 1) with your info.
4. Compile java project with code from server folder
5. Run server (from either ide or exported jar)
6. Set your homepage to localhost:4040


Currently there are many bugs in both JavaScript and Java code.
This page is nowhere near finished.
Since this is personal project I would not like anyone but me working on it, but feel free to fork it and edit it as you wish. :)